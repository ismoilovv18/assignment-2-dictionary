import subprocess

inputs = [
"first", 
"second", 
"third", 
"fourth", 
"fifth", 
"", 
"random", 
"Hello world"
]

outputs = [
"value 1", 
"value 2", 
"value 3", 
"value 4", 
"value 5", 
"", 
"", 
""
]

errors = [
"", 
"", 
"", 
"", 
"", 
"!!! not found !!!", 
"!!! not found !!!", 
"!!! key should be < 256 chars !!!"
]

num_failures = 0

print("Starting tests")
print("-------------------")

for i in range(len(inputs)):
    process = subprocess.Popen(["./result"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=inputs[i].encode())

    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()

    if stdout == outputs[i] and stderr == errors[i]:
        print("Test %s passed" % i)
    else:
        num_failures += 1
        print("Test %s failed" % i)
        if stdout != outputs[i]:
            print("Wrong stdout " + stdout + ", expected " + outputs[i])
        if stderr != errors[i]:
            print("Wrong stderr " + stderr + " expected " + errors[i])
    print("-------------------")

if num_failures == 0:
    print("All tests passed")
else:
    print("%d tests failed" % num_failures)
